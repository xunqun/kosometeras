package com.kdanmobile.kosometer.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by snippets on 14/12/2.
 */
public class RawData {

    // JSON value
    public static final String JSON_VALUE = "JSON_VALUE";
    public static final String JSON_TIME = "JSON_TIME";

    String rawdata;
    long timestamp;

    public RawData(String rawdata, long timestamp) {
        this.rawdata = rawdata;
        this.timestamp = timestamp;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject j = new JSONObject();
        j.put(JSON_TIME, timestamp);
        j.put(JSON_VALUE, rawdata);
        return j;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getRawdata() {
        return rawdata;
    }
}
