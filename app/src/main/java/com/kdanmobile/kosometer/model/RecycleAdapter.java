package com.kdanmobile.kosometer.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.fingerchart.ChartView;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by snippets on 14/12/2.
 */
public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder> {

    private static String TAG = "RecycleAdapter";

    private static final int POSITION_SPEED = 0;
    private static final int POSITION_RPM = 1;
    private static final int POSITION_MILEAGE = 2;
    private static final int POSITION_TOTAL_MILEAGE = 3;
    private static final int POSITION_AFRATIO = 4;
    private static final int POSITION_TEMPRATURE = 5;
    private static final int POSITION_VOLTAGE = 6;


    private Context mContext;
    private RecordManager.RecordSet mRecordSet;

    private ChartView.OnSelectedXListener mOnSelectedXListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mView;

        public ViewHolder(View v) {
            super(v);
            mView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecycleAdapter(Context context, RecordManager.RecordSet recordSet) {
        mContext = context;
        mRecordSet = recordSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_recycler_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        int titleRes = R.string.speed;
        switch (position) {
            case POSITION_SPEED:
                titleRes = R.string.speed;
                break;
            case POSITION_RPM:
                titleRes = R.string.rpm;
                break;
            case POSITION_MILEAGE:
                titleRes = R.string.mileage;
                break;
            case POSITION_TOTAL_MILEAGE:
                titleRes = R.string.total_mileage;
                break;
            case POSITION_AFRATIO:
                titleRes = R.string.af_ratio;
                break;
            case POSITION_TEMPRATURE:
                titleRes = R.string.temprature;
                break;
            case POSITION_VOLTAGE:
                titleRes = R.string.voltage;
                break;
        }


        ((TextView) holder.mView.findViewById(R.id.textView)).setText(mContext.getString(titleRes));
        ChartView chart = (ChartView) holder.mView.findViewById(R.id.chart);
        ArrayList<ChartView.ChartPoint> list = initChartPoint(position);
        chart.setChartPoints(list);
        if (mOnSelectedXListener != null) {
            chart.setOnSelectedXListener(new ChartView.OnSelectedXListener() {
                @Override
                public void onSelected(double x, ChartView.ChartPoint point) {
                    if (mOnSelectedXListener != null) {
                        Calendar cl = Calendar.getInstance();
                        cl.setTimeInMillis((long) point.x);
                        String timeString = cl.get(Calendar.HOUR_OF_DAY) + " : " + cl.get(Calendar.MINUTE) + ":" + cl.get(Calendar.SECOND);

                        mOnSelectedXListener.onSelected(x, point);
                        ((TextView) holder.mView.findViewById(R.id.value)).setText(String.valueOf(point.y));
                        ((TextView) holder.mView.findViewById(R.id.time)).setText(timeString);
                    }
                }

                @Override
                public void onActionUp() {
                    ((TextView) holder.mView.findViewById(R.id.value)).setText("");
                    ((TextView) holder.mView.findViewById(R.id.time)).setText("");
                }
            });
        }
    }


    public void setOnSelectedXListener(ChartView.OnSelectedXListener l) {
        mOnSelectedXListener = l;
    }

    public ArrayList<ChartView.ChartPoint> initChartPoint(int position) {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();

        switch (position) {
            case POSITION_SPEED:
                values = speedsToPoint();
                break;
            case POSITION_RPM:
                values = rpmsToPoint();
                break;
            case POSITION_MILEAGE:
                values = mileagesToPoint();
                break;
            case POSITION_TOTAL_MILEAGE:
                values = totalMileagesToPoint();
                break;
            case POSITION_AFRATIO:
                values = afRatiosToPoint();
                break;
            case POSITION_TEMPRATURE:
                values = tempraturesToPoint();
                break;
            case POSITION_VOLTAGE:
                values = voltageToPoint();
                break;
        }
        return values;
    }


    private ArrayList<ChartView.ChartPoint> voltageToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.VoltageData> data = mRecordSet.getVoltages();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.VoltageData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;
    }

    private ArrayList<ChartView.ChartPoint> tempraturesToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.TempratureData> data = mRecordSet.getTempratures();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.TempratureData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;

    }

    private ArrayList<ChartView.ChartPoint> afRatiosToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.AfRatioData> data = mRecordSet.getAfRatios();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.AfRatioData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;
    }

    private ArrayList<ChartView.ChartPoint> totalMileagesToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.TotalMileageData> data = mRecordSet.getTotalMileages();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.TotalMileageData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;
    }

    private ArrayList<ChartView.ChartPoint> mileagesToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.MileageData> data = mRecordSet.getMileages();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.MileageData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;
    }

    private ArrayList<ChartView.ChartPoint> rpmsToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.RpmData> data = mRecordSet.getRpms();

        for (int i = 0; i < data.size(); i++) {
            RecordManager.RpmData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));
        }
        return values;
    }

    private ArrayList<ChartView.ChartPoint> speedsToPoint() {
        ArrayList<ChartView.ChartPoint> values = new ArrayList<ChartView.ChartPoint>();
        ArrayList<RecordManager.SpeedData> data = mRecordSet.getSpeeds();
        for (int i = 0; i < data.size(); i++) {
            RecordManager.SpeedData d = data.get(i);
            values.add(new ChartView.ChartPoint(d.timestamp, d.getValue()));

        }

        return values;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return 7;
    }
}
