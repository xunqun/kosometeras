package com.kdanmobile.kosometer.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by snippets on 14/12/2.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "kosometer.db";

    private static SQLiteDatabase mDb;
    private static DbHelper mHelper;
    private Context mContext;

    public static class Record {
        public static String TABLE_NAME = "RECORD";
        public static String _ID = "_ID";
        public static String _DATA = "_NAME";
        public static String _TIMESTAMP = "_TIMESTAMP";
    }

    public static class TempRawData {
        public static String TABLE_NAME = "TEMPRAWDATA";
        public static String _ID = "_ID";
        public static String _TYPE = "_TYPE";
        public static String _VALUE = "_VALUE";
        public static String _TIMESTAMP = "_TIMESTAMP";

    }

    // SQL create table commands
    private final String SQL_CREATE_RECORD = "CREATE TABLE " + Record.TABLE_NAME + " (" + Record._ID + " INTEGER PRIMARY KEY autoincrement," + Record._DATA + " TEXT NOT NULL, " + Record._TIMESTAMP + " NUMERIC " + ");";
    private final String SQL_CREATE_TEMPRAWDATA = "CREATE TABLE " + TempRawData.TABLE_NAME + " (" +
            TempRawData._ID + " INTEGER PRIMARY KEY autoincrement," +
            TempRawData._TYPE + "  TEXT NOT NULL, " +
            TempRawData._VALUE + "  TEXT NOT NULL, " +
            TempRawData._TIMESTAMP + " NUMERIC " + ");";

    // Constructure
    public DbHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        mContext = context;

    }

    public static SQLiteDatabase getDb(Context c) {
        if (mDb == null) {
            initDB(c);
        }
        return mDb;
    }

    private static void initDB(Context c) {
        try {
            mHelper = new DbHelper(c, null);
            mDb = mHelper.getWritableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeDb() {
        if (mDb != null && mDb.isOpen()) {
            mDb.close();
            mDb = null;
        } else if (mDb != null) {
            mDb = null;
        }
    }

    public static void storeRecord(Context context, String data) {

        long time = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(Record._DATA, data);
        values.put(Record._TIMESTAMP, time);

        getDb(context).insert(Record.TABLE_NAME, null, values);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_RECORD);
        db.execSQL(SQL_CREATE_TEMPRAWDATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}