package com.kdanmobile.kosometer.model;

import android.content.Context;
import android.database.Cursor;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by snippets on 14/12/4.
 */
public class RecordManager {

    // data name
    public static String PROPERTY_BEGIN_TIME = "PROPERTY_BEGIN_TIME";
    public static String PROPERTY_LAST_TIME = "PROPERTY_LAST_TIME";
    public static String PROPERTY_DURATION = "PROPERTY_DURATION";
    public static String DATA_LOCATION = "DATA_LOCATION";
    public static String DATA_SPEED = "DATA_SPEED";
    public static String DATA_RPM = "DATA_RPM";
    public static String DATA_MILEAGE = "DATA_MILEAGE";
    public static String DATA_TOTAL_MILEAGE = "DATA_TOTAL_MILEAGE";
    public static String DATA_TEMPRATURE = "DATA_TEMPRATURE";
    public static String DATA_AF_RATIO = "DATA_AF_RATIO";
    public static String DATA_VOLTAGE = "DATA_VOLTAGE";

    private static RecordManager instance;
    private static ArrayList<RecordSet> mRecordSets = null;
    private ArrayList<OnRecordSetChangedListener> mListeners = new ArrayList<OnRecordSetChangedListener>();

    public interface OnRecordSetChangedListener {
        public void onChanged();
    }

    public static RecordManager get(Context context) {

        if (instance == null) {
            instance = new RecordManager();
            instance.initInstance(context);
        }
        return instance;
    }

    public static void recycle(){
        instance = null;
    }

    private void initInstance(Context context) {
        mRecordSets = new ArrayList<RecordSet>();
        String[] colums = {DbHelper.Record._DATA, DbHelper.Record._ID, DbHelper.Record._TIMESTAMP};
        Cursor c = DbHelper.getDb(context).query(DbHelper.Record.TABLE_NAME, colums, "", new String[]{}, "", "", "");
        while (c.moveToNext()) {
            String data = c.getString(c.getColumnIndex(DbHelper.Record._DATA));
            long timestamp = c.getLong(c.getColumnIndex(DbHelper.Record._TIMESTAMP));
            long id = c.getLong(c.getColumnIndex(DbHelper.Record._ID));
            try {
                JSONObject json = new JSONObject(data);
                RecordSet set = new RecordSet(json, timestamp, id);
                mRecordSets.add(set);
                notifyListeners();
            } catch (Exception e) {

            }

        }

    }

    public void deleteRecord(Context context, long id) {
        int count = DbHelper.getDb(context).delete(DbHelper.Record.TABLE_NAME, DbHelper.Record._ID + "=?", new String[]{String.valueOf(id)});
        if (count > 0)
            initInstance(context);
    }

    public void cleanAll(Context context){
        int count = DbHelper.getDb(context).delete(DbHelper.Record.TABLE_NAME, "", new String[]{});
        initInstance(context);
        notifyListeners();
    }

    private void notifyListeners() {
        for (int i = 0; i < mListeners.size(); i++) {
            OnRecordSetChangedListener l = mListeners.get(i);
            l.onChanged();
        }
    }

    public void notifyDatasetChanged(Context context) {
        if (instance == null) {
            instance = new RecordManager();
        }
        instance.initInstance(context);
    }

    public void registerOnRecordSetChangedListener(OnRecordSetChangedListener listener) {
        mListeners.add(listener);
    }

    public void unregisterOnRecordSetChangedListener(OnRecordSetChangedListener listener) {
        mListeners.remove(listener);
    }


    //Data class
    public static class RecordSet {

        private long id;
        private long timestamp;
        private long beginTime;
        private long lastTime;
        private long duration;

        // Data list
        ArrayList<LocationData> locations = new ArrayList<LocationData>();
        ArrayList<SpeedData> speeds = new ArrayList<SpeedData>();
        ArrayList<RpmData> rpms = new ArrayList<RpmData>();
        ArrayList<MileageData> mileages = new ArrayList<MileageData>();
        ArrayList<TotalMileageData> totalMileages = new ArrayList<TotalMileageData>();
        ArrayList<TempratureData> tempratures = new ArrayList<TempratureData>();
        ArrayList<VoltageData> voltages = new ArrayList<VoltageData>();
        ArrayList<AfRatioData> afRatios = new ArrayList<AfRatioData>();

        public RecordSet(JSONObject json, long timestamp, long id) {
            this.id = id;
            this.timestamp = timestamp;
            try {

                beginTime = json.getLong(PROPERTY_BEGIN_TIME);
                lastTime = json.getLong(PROPERTY_LAST_TIME);
                duration = json.getLong(PROPERTY_DURATION);

                /*
                // Retrieve Location
                JSONArray array = json.getJSONArray(DATA_LOCATION);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    LocationData d = new LocationData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    locations.add(d);
                }

                // Retrieve Speed
                array = json.getJSONArray(DATA_SPEED);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    SpeedData d = new SpeedData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    speeds.add(d);
                }

                // Retrieve Rpm
                array = json.getJSONArray(DATA_RPM);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    RpmData d = new RpmData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    rpms.add(d);
                }

                // Retrieve Mileage
                array = json.getJSONArray(DATA_MILEAGE);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    MileageData d = new MileageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    mileages.add(d);
                }

                // Retrieve Total Mileage
                array = json.getJSONArray(DATA_TOTAL_MILEAGE);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    TotalMileageData d = new TotalMileageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    totalMileages.add(d);
                }

                // Retrieve Temprature
                array = json.getJSONArray(DATA_TEMPRATURE);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    TempratureData d = new TempratureData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    tempratures.add(d);
                }

                // Retrieve AF ratio
                array = json.getJSONArray(DATA_AF_RATIO);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    AfRatioData d = new AfRatioData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    afRatios.add(d);
                }

                // Retrieve Voltage
                array = json.getJSONArray(DATA_VOLTAGE);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    VoltageData d = new VoltageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                    voltages.add(d);
                }
                */

            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                json = null;
            }
        }

        public void retriveData(Context context){
            String[] colums = {DbHelper.Record._DATA, DbHelper.Record._ID, DbHelper.Record._TIMESTAMP};
            Cursor c = DbHelper.getDb(context).query(DbHelper.Record.TABLE_NAME, colums, DbHelper.Record._ID + "=?", new String[]{String.valueOf(id)}, "", "", "");
            if(c.moveToNext()){
                String data = c.getString(c.getColumnIndex(DbHelper.Record._DATA));
                try {
                    JSONObject json = new JSONObject(data);
                    retriveDataFromJson(json);
                }catch (Exception e){

                }
            }
        }

        private void retriveDataFromJson(JSONObject json) throws JSONException{
            // Retrieve Location
            JSONArray array = json.getJSONArray(DATA_LOCATION);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                LocationData d = new LocationData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                locations.add(d);
            }

            // Retrieve Speed
            array = json.getJSONArray(DATA_SPEED);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                SpeedData d = new SpeedData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                speeds.add(d);
            }

            // Retrieve Rpm
            array = json.getJSONArray(DATA_RPM);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                RpmData d = new RpmData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                rpms.add(d);
            }

            // Retrieve Mileage
            array = json.getJSONArray(DATA_MILEAGE);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                MileageData d = new MileageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                mileages.add(d);
            }

            // Retrieve Total Mileage
            array = json.getJSONArray(DATA_TOTAL_MILEAGE);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                TotalMileageData d = new TotalMileageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                totalMileages.add(d);
            }

            // Retrieve Temprature
            array = json.getJSONArray(DATA_TEMPRATURE);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                TempratureData d = new TempratureData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                tempratures.add(d);
            }

            // Retrieve AF ratio
            array = json.getJSONArray(DATA_AF_RATIO);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                AfRatioData d = new AfRatioData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                afRatios.add(d);
            }

            // Retrieve Voltage
            array = json.getJSONArray(DATA_VOLTAGE);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                VoltageData d = new VoltageData(obj.getString(RawData.JSON_VALUE), obj.getLong(RawData.JSON_TIME));
                voltages.add(d);
            }
            array = null;
        }

        public void dataRecycle(){
            locations.clear();
            speeds.clear();
            rpms.clear();
            mileages.clear();
            totalMileages.clear();
            tempratures.clear();
            afRatios.clear();
            voltages.clear();
        }

        // Getter & Setter


        public long getId() {
            return id;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
        }

        public long getBeginTime() {
            return beginTime;
        }

        public long getLastTime() {
            return lastTime;
        }

        public double getDuration() {
            return duration;
        }

        public ArrayList<LocationData> getLocations() {
            return locations;
        }

        public ArrayList<SpeedData> getSpeeds() {
            return speeds;
        }

        public ArrayList<RpmData> getRpms() {
            return rpms;
        }

        public ArrayList<MileageData> getMileages() {
            return mileages;
        }

        public ArrayList<TotalMileageData> getTotalMileages() {
            return totalMileages;
        }

        public ArrayList<TempratureData> getTempratures() {
            return tempratures;
        }

        public ArrayList<VoltageData> getVoltages() {
            return voltages;
        }

        public ArrayList<AfRatioData> getAfRatios() {
            return afRatios;
        }
    }

    // Getter & Setter


    public ArrayList<RecordSet> getRecordSets() {
        return mRecordSets;
    }

    public static class LocationData extends RawData {
        private LatLng value;

        public LocationData(String raw, long timestamp) {
            super(raw, timestamp);
            String[] data = raw.split(",");
            try {
                value = new LatLng(Double.valueOf(data[0]), Double.valueOf(data[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public LatLng getValue() {
            return value;
        }
    }

    public static class SpeedData extends RawData {
        private int value;

        public SpeedData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }

    }

    public static class RpmData extends RawData {
        private int value;

        public RpmData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static class MileageData extends RawData {
        private int value;

        public MileageData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static class TotalMileageData extends RawData {
        private int value;

        public TotalMileageData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static class TempratureData extends RawData {
        private int value = 0;

        public TempratureData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static class AfRatioData extends RawData {
        private int value = 0;

        public AfRatioData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public int getValue() {
            return value;
        }
    }

    public static class VoltageData extends RawData {
        private int value = 0;

        public VoltageData(String raw, long timestamp) {
            super(raw, timestamp);
            try {
                value = Integer.valueOf(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getValue() {
            return value;
        }
    }

}
