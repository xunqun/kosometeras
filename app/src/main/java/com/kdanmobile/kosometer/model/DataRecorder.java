package com.kdanmobile.kosometer.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by snippets on 14/12/2.
 */
public class DataRecorder {
    private String TAG = "DataRecorder";
    private int SAMPLE_COUNT = 5000;
    private final int RECORD_INTERVAL_MILLSECOND = 10;
    private static DataRecorder instance;

    // Data list
    DataSet locations = new DataSet();
    DataSet speeds = new DataSet();
    DataSet rpms = new DataSet();
    DataSet mileages = new DataSet();
    DataSet totalMileages = new DataSet();
    DataSet tempratures = new DataSet();
    DataSet voltages = new DataSet();
    DataSet afRatios = new DataSet();

    // Properties
    private long firstTime = -1;
    private long lastTime = -1;

    public static DataRecorder get() {
        if (instance == null) {
            instance = new DataRecorder();
        }
        return instance;
    }


    public void storeToDb(final Context context) throws JSONException {
        new AsyncTask<Void, Void, Void>() {

            DataSet sampledLocations = new DataSet();
            DataSet sampledSpeeds = new DataSet();
            DataSet sampledRpms = new DataSet();
            DataSet sampledMileages = new DataSet();
            DataSet sampledTotalMileages = new DataSet();
            DataSet sampledTempratures = new DataSet();
            DataSet sampledVoltages = new DataSet();
            DataSet sampledAfRatios = new DataSet();

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    DbHelper.storeRecord(context, toJson().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }


            private JSONObject toJson() throws JSONException {
                sampleDataSetFromDb();
                return encodeAsJSON();
            }

            private JSONObject encodeAsJSON() throws JSONException{
                JSONObject jsonObject = new JSONObject();

                jsonObject.put(RecordManager.PROPERTY_BEGIN_TIME, firstTime);
                jsonObject.put(RecordManager.PROPERTY_LAST_TIME, lastTime);

                // millisecond
                jsonObject.put(RecordManager.PROPERTY_DURATION, (lastTime - firstTime));

                // Add location
                JSONArray array = new JSONArray();
                for (int i = 0; i < sampledLocations.size(); i++) {
                    RecordManager.LocationData d = (RecordManager.LocationData) sampledLocations.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_LOCATION, array);

                // Add speed
                array = new JSONArray();
                for (int i = 0; i < sampledSpeeds.size(); i++) {
                    RecordManager.SpeedData d = (RecordManager.SpeedData) sampledSpeeds.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_SPEED, array);

                // Add rpm
                array = new JSONArray();
                for (int i = 0; i < sampledRpms.size(); i++) {
                    RecordManager.RpmData d = (RecordManager.RpmData) sampledRpms.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_RPM, array);

                // Add mileage
                array = new JSONArray();
                for (int i = 0; i < sampledMileages.size(); i++) {
                    RecordManager.MileageData d = (RecordManager.MileageData) sampledMileages.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_MILEAGE, array);

                // Add total mileage
                array = new JSONArray();
                for (int i = 0; i < sampledTotalMileages.size(); i++) {
                    RecordManager.TotalMileageData d = (RecordManager.TotalMileageData) sampledTotalMileages.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_TOTAL_MILEAGE, array);

                // Add af ratio
                array = new JSONArray();
                for (int i = 0; i < sampledAfRatios.size(); i++) {
                    RecordManager.AfRatioData d = (RecordManager.AfRatioData) sampledAfRatios.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_AF_RATIO, array);

                // Add tempration
                array = new JSONArray();
                for (int i = 0; i < sampledTempratures.size(); i++) {
                    RecordManager.TempratureData d = (RecordManager.TempratureData) sampledTempratures.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_TEMPRATURE, array);

                // Add af voltage
                array = new JSONArray();
                for (int i = 0; i < sampledVoltages.size(); i++) {
                    RecordManager.VoltageData d = (RecordManager.VoltageData) sampledVoltages.get(i);
                    array.put(d.toJson());
                }
                jsonObject.put(RecordManager.DATA_VOLTAGE, array);

                Log.d(TAG, jsonObject.toString());
                return jsonObject;
            }

            private void sampleDataSetFromDb() {
                retriveDataFromDb(RecordManager.DATA_LOCATION);
                retriveDataFromDb(RecordManager.DATA_SPEED);
                retriveDataFromDb(RecordManager.DATA_RPM);
                retriveDataFromDb(RecordManager.DATA_AF_RATIO);
                retriveDataFromDb(RecordManager.DATA_MILEAGE);
                retriveDataFromDb(RecordManager.DATA_TOTAL_MILEAGE);
                retriveDataFromDb(RecordManager.DATA_TEMPRATURE);
                retriveDataFromDb(RecordManager.DATA_VOLTAGE);
            }

            private void retriveDataFromDb(String type) {
                String[] columns = new String[]{DbHelper.TempRawData._VALUE, DbHelper.TempRawData._TYPE, DbHelper.TempRawData._TIMESTAMP};
                Cursor c = DbHelper.getDb(context).query(DbHelper.TempRawData.TABLE_NAME, columns, DbHelper.TempRawData._TYPE + "=?", new String[]{type}, "", "", "");
                //int sampleRate = (int)(c.getCount()/SAMPLE_COUNT);
                int sampleRate = 1;
                int count = 0;
                while (c.moveToNext()) {

                    if(count >= sampleRate) {
                        retrievingToList(type,c);
                        count = 0;
                    }else{
                        count ++;
                    }
                }
            }




            private void retrievingToList(String type, Cursor c) {
                String value = c.getString(c.getColumnIndex(DbHelper.TempRawData._VALUE));
                long timestamp = c.getLong(c.getColumnIndex(DbHelper.TempRawData._TIMESTAMP));

                if (type.equals(RecordManager.DATA_LOCATION)) {
                    sampledLocations.add(new RecordManager.LocationData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_AF_RATIO)) {
                    sampledAfRatios.add(new RecordManager.AfRatioData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_MILEAGE)) {
                    sampledMileages.add(new RecordManager.MileageData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_RPM)) {
                    sampledRpms.add(new RecordManager.RpmData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_SPEED)) {

                    sampledSpeeds.add(new RecordManager.SpeedData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_TEMPRATURE)) {
                    sampledTempratures.add(new RecordManager.TempratureData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_TOTAL_MILEAGE)) {
                    sampledTotalMileages.add(new RecordManager.TotalMileageData(value, timestamp));
                } else if (type.equals(RecordManager.DATA_VOLTAGE)) {
                    sampledVoltages.add(new RecordManager.VoltageData(value, timestamp));
                }
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                RecordManager.get(context).notifyDatasetChanged(context);
                DbHelper.getDb(context).delete(DbHelper.TempRawData.TABLE_NAME,"",new String[]{});
            }
        }.execute();


    }

    public void saveBufferListToDbAsync(final Context context, final String type, final ArrayList<RawData> rawDatas) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    ContentValues values;
                    for (RawData d : rawDatas) {

                        values = new ContentValues();
                        values.put(DbHelper.TempRawData._TIMESTAMP, d.getTimestamp());
                        values.put(DbHelper.TempRawData._TYPE, type);
                        values.put(DbHelper.TempRawData._VALUE, d.getRawdata());
                        DbHelper.getDb(context).insert(DbHelper.TempRawData.TABLE_NAME, null, values);
                    }
                    Log.d(TAG, "store " + type + " " + rawDatas.size());
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }
        }.execute();
    }

    long pretime = 0;
    int predvalue = 0;
    public void addData(Context context, String dataType, String data) {
        long timeStamp = System.currentTimeMillis();
        if (firstTime < 0) {
            firstTime = timeStamp;
        }

        if (dataType.equals(RecordManager.DATA_LOCATION)) {
            addToBufferList(context, dataType, new RecordManager.LocationData(data, timeStamp), locations);
        } else if (dataType.equals(RecordManager.DATA_RPM)) {
            addToBufferList(context, dataType, new RecordManager.RpmData(data, timeStamp), rpms);
        } else if (dataType.equals(RecordManager.DATA_SPEED)) {
            addToBufferList(context, dataType, new RecordManager.SpeedData(data, timeStamp), speeds);
        } else if (dataType.equals(RecordManager.DATA_AF_RATIO)) {
            addToBufferList(context, dataType, new RecordManager.AfRatioData(data, timeStamp), afRatios);
        } else if (dataType.equals(RecordManager.DATA_MILEAGE)) {
            addToBufferList(context, dataType, new RecordManager.MileageData(data, timeStamp), mileages);
        } else if (dataType.equals(RecordManager.DATA_TOTAL_MILEAGE)) {
            addToBufferList(context, dataType, new RecordManager.TotalMileageData(data, timeStamp), totalMileages);
        } else if (dataType.equals(RecordManager.DATA_TEMPRATURE)) {
            addToBufferList(context, dataType, new RecordManager.TempratureData(data, timeStamp), tempratures);
        } else if (dataType.equals(RecordManager.DATA_VOLTAGE)) {
            addToBufferList(context, dataType, new RecordManager.VoltageData(data, timeStamp), voltages);
        }
    }



    public void addToBufferList(Context context, String type, RawData data, ArrayList list) {


        list.add(data);
        if (list.size() > 100) {
            saveBufferListToDbAsync(context, type, (ArrayList<RawData>) list.clone());
            list.clear();
        }
    }

    /**
     * Stop recording and save the whole record to DB
     */
    public void stop(Context context) throws JSONException {
        // If there have any data, then store it

        if (firstTime > 0) {
            lastTime = System.currentTimeMillis();

            saveBufferListToDbAsync(context, RecordManager.DATA_AF_RATIO, afRatios);
            saveBufferListToDbAsync(context, RecordManager.DATA_LOCATION, locations);
            saveBufferListToDbAsync(context, RecordManager.DATA_MILEAGE, mileages);
            saveBufferListToDbAsync(context, RecordManager.DATA_RPM, rpms);
            saveBufferListToDbAsync(context, RecordManager.DATA_SPEED, speeds);
            saveBufferListToDbAsync(context, RecordManager.DATA_TEMPRATURE, tempratures);
            saveBufferListToDbAsync(context, RecordManager.DATA_TOTAL_MILEAGE, totalMileages);
            saveBufferListToDbAsync(context, RecordManager.DATA_VOLTAGE, voltages);

            storeToDb(context);
        }


    }

    private class DataSet extends ArrayList<RawData> {
        long lastTimeStamp = 0;

        @Override
        public boolean add(RawData object) {

            // Control the data saving rate
            long elapsedTime = object.getTimestamp() - lastTimeStamp;

            if (elapsedTime > RECORD_INTERVAL_MILLSECOND) {
                lastTimeStamp = object.getTimestamp();
                boolean result = super.add(object);
                return result;
            } else {
                return false;
            }

        }

    }


}
