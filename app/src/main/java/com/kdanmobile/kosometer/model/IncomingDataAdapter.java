package com.kdanmobile.kosometer.model;

/**
 * Created by snippets on 14/12/4.
 */
public class IncomingDataAdapter {

    private byte[] mRawData;
    private String speed;
    private String rpm;
    private String mileage;
    private String totalMileage;
    private String temprature;
    private String airFuelRatio;
    private String voltage;

    public IncomingDataAdapter(byte[] b) {
        mRawData = b;
        parseData();
    }

    private void parseData() {
        speed = retrieveSpeed();
        rpm = retrieveRpm();
        mileage = retrieveMileage();
        totalMileage = retrieveTotalMaleage();
        temprature = retrieveTemprature();
        airFuelRatio = retrieveAirFuelRate();
        voltage = retrieveVoltage();
    }

    private String retrieveVoltage() {
        byte[] data = new byte[1];
        data[0] = mRawData[17];
        return bcdToString(data);
    }

    private String retrieveAirFuelRate() {
        byte[] data = new byte[2];
        data[0] = mRawData[15];
        data[1] = mRawData[16];
        return bcdToString(data);
    }

    private String retrieveTemprature() {
        byte[] data = new byte[2];
        data[0] = mRawData[13];
        data[1] = mRawData[14];
        return bcdToString(data);
    }

    private String retrieveTotalMaleage() {
        byte[] data = new byte[3];
        data[0] = mRawData[10];
        data[1] = mRawData[11];
        data[2] = mRawData[12];
        return bcdToString(data);
    }

    private String retrieveMileage() {
        byte[] data = new byte[2];
        data[0] = mRawData[8];
        data[1] = mRawData[9];
        return bcdToString(data);
    }

    private String retrieveSpeed() {
        byte[] speed = new byte[2];
        speed[0] = mRawData[4];
        speed[1] = mRawData[5];
        return bcdToString(speed);
    }

    private String retrieveRpm() {
        byte[] rpm = new byte[2];
        rpm[0] = mRawData[6];
        rpm[1] = mRawData[7];
        return bcdToString(rpm);
    }

    private String bcdToString(byte[] bcdDigits) {
        StringBuilder sb = new StringBuilder(bcdDigits.length * 2);
        for (byte b : bcdDigits) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    // getter & setter

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getRpm() {
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getTotalMileage() {
        return totalMileage;
    }

    public void setTotalMileage(String totalMileage) {
        this.totalMileage = totalMileage;
    }

    public String getTemprature() {
        return temprature;
    }

    public void setTemprature(String temprature) {
        this.temprature = temprature;
    }

    public String getAirFuelRatio() {
        return airFuelRatio;
    }

    public void setAirFuelRatio(String airFuelRatio) {
        this.airFuelRatio = airFuelRatio;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }
}
