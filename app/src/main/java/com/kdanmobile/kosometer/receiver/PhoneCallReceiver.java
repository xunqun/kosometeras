package com.kdanmobile.kosometer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by snippets on 14/12/11.
 */
public class PhoneCallReceiver extends BroadcastReceiver {
    public static final String TAG = "PhoneCallReceiver";
    public static final String ACTION_BROADCAST_PHONECALL = "ACTION_BROADCAST_PHONECALL";
    public static final String EXTRA_BROADCAST_RINGING = "EXTRA_BROADCAST_RINGING";
    public static final String EXTRA_BROADCAST_NUM = "EXTRA_BROADCAST_NUM";
    TelephonyManager telephony;

    @Override
    public void onReceive(Context context, Intent intent) {
        MyPhoneStateListener phoneListener = new MyPhoneStateListener(context);
        telephony = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public void onDestroy() {
        telephony.listen(null, PhoneStateListener.LISTEN_NONE);
    }




    public static class MyPhoneStateListener extends PhoneStateListener {

        public Context mContext;
        public static Boolean phoneRinging = false;
        public String number;

        public MyPhoneStateListener(Context context){
            mContext = context;
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            number = incomingNumber;
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.d(TAG, "IDLE");
                    if(phoneRinging)
                        broadcast(false);
                    phoneRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d(TAG, "OFFHOOK");
                    if(phoneRinging)
                        broadcast(false);
                    phoneRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d(TAG, "RINGING");
                    if(!phoneRinging)
                        broadcast(true);
                    phoneRinging = true;

                    break;
            }
        }

        public void broadcast(boolean ringing) {
            Intent it = new Intent(ACTION_BROADCAST_PHONECALL);
            it.putExtra(EXTRA_BROADCAST_RINGING, ringing);
            it.putExtra(EXTRA_BROADCAST_NUM, number);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(it);
        }

    }

}
