package com.kdanmobile.kosometer.bluetooth;

/**
 * Created by snippets on 14/11/28.
 */
public class DeviceState {
    private static DeviceState instance;

    // The state is defined; in BluetoothService
    private int connectState = BluetoothService.STATE_NONE;
    private byte[] incomingBuffer;
    private String deviceName;
    private String deviceMac;
    private int incomingByte;

    public static DeviceState get() {
        if (instance == null) {
            instance = init();
        }
        return instance;
    }

    private static DeviceState init() {
        DeviceState state = new DeviceState();
        return state;
    }

    public int getConnectState() {
        return connectState;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceMac() {
        return deviceMac;
    }

    public byte[] getIncomingBuffer() {
        return incomingBuffer;
    }

    public int getIncomingByte() {
        return incomingByte;
    }
// Getter

    public void setConnectState(int connected) {
        this.connectState = connected;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public void setIncomingBuffer(byte[] incomingBuffer) {
        this.incomingBuffer = incomingBuffer;
    }

    public void setIncomingByte(int incomingByte) {
        this.incomingByte = incomingByte;
    }


}
