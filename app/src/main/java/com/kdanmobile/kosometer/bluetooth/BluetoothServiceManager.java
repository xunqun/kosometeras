package com.kdanmobile.kosometer.bluetooth;

import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.activity.MainActivity;
import com.kdanmobile.kosometer.model.DataRecorder;
import com.kdanmobile.kosometer.model.IncomingDataAdapter;
import com.kdanmobile.kosometer.model.RecordManager;
import com.kdanmobile.kosometer.receiver.PhoneCallReceiver;
import com.kdanmobile.kosometer.utils.BCD;
import com.kdanmobile.kosometer.utils.BinHex;
import com.kdanmobile.kosometer.utils.DataCheck;
import com.kdanmobile.kosometer.utils.PreferenceManager;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by snippets on 14/11/28.
 */
public class BluetoothServiceManager extends Service implements LocationListener {

    // Defined for the foreground notification
    public static int ONGOING_NOTIFICATION_ID = 19;



    // Defines the extra name and value for incoming intents
    public static final String EXTRA_ACTION = "EXTRA_ACTION";
    public static final String EXTRA_MAC = "EXTRA_MAC";
    public static final String ACTION_START = "ACTION_START";
    public static final String ACTION_STOP = "ACTION_STOP";


    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothService Handler

    public static final String TOAST = "toast";

    // Broadcase actions
    public static final String ACTION_BROADCAST_DATA = "com.kdanmobile.kosometer.BROADCAST";
    public static final String EXTRA_BROADCAST_MESSAGE = "EXTRA_BROADCAST_TYPE";
    public static final String EXTRA_BROADCAST_DATA = "EXTRA_BROADCAST_DATA";

    // Location event
    public static final int MESSAGE_ONLOCATION_CHANGE = 10;
    public static final int MESSAGE_ONPROVIDER_DISABLEED = 11;
    public static final int MESSAGE_ONPROVIDER_ENABLED = 12;
    public static final int MESSAGE_ONSTATUS_CHANGED = 13;

    // Debug
    boolean D = true;
    public final String TAG = "BluetoothServiceManager";

    // Defines the state of running thread
    BluetoothService mBtService;
    public boolean running = false;
    Thread mThread;

    // Bluetooth
    public BluetoothAdapter mBluetoothAdapter;
    public String strMac;
    private IncomingDataAdapter mIncomingDataAdapter;
    byte[] storeBuffer = new byte[20];
    int receiveByte;
    int storeLength = 0;

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (D)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            showForgroundNotification();
                            DeviceState.get().setConnectState(BluetoothService.STATE_CONNECTED);
                            // Start location listener
                            requestLocationUpdates();
                            requestTimeAlign();
                            requestDataUpdate();

                            break;
                        case BluetoothService.STATE_CONNECTING:
                            DeviceState.get().setConnectState(BluetoothService.STATE_CONNECTING);
                            break;
                        // case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            DeviceState.get().setConnectState(BluetoothService.STATE_NONE);
                            stopSelf();
                            break;
                    }
                    broadcast(MESSAGE_STATE_CHANGE);
                    break;
                case MESSAGE_READ:

                    byte[] readBuf = (byte[]) msg.obj;
                    for (int i = 0; i < msg.arg1; i++) {
                        receiveByte = (byte) readBuf[i];
                        if (receiveByte == (byte) 0xAA ||
                                receiveByte == (byte) 0xDD) {

                            storeLength = 0;
                            storeBuffer[storeLength++] = (byte) receiveByte;
                        } else if (readBuf[i] == 0x0D) {

                            //手機下達指令，MCU回傳資料處理程式段
                            if (storeLength == 19) {
                                storeBuffer[storeLength] = readBuf[i];
//                                Log.i(TAG, "Read from MCU: " + BinHex.byteArrayToHex(storeBuffer, 20));
                                DeviceState.get().setIncomingBuffer(storeBuffer.clone());
                                DeviceState.get().setIncomingByte(msg.arg1);

                                mIncomingDataAdapter = new IncomingDataAdapter(DeviceState.get().getIncomingBuffer().clone());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_SPEED, mIncomingDataAdapter.getSpeed());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_RPM, mIncomingDataAdapter.getRpm());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_AF_RATIO, mIncomingDataAdapter.getAirFuelRatio());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_MILEAGE, mIncomingDataAdapter.getMileage());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_TOTAL_MILEAGE, mIncomingDataAdapter.getTotalMileage());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_TEMPRATURE, mIncomingDataAdapter.getTemprature());
                                DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_VOLTAGE, mIncomingDataAdapter.getVoltage());
                                broadcast(MESSAGE_READ);
                            } else if (storeLength == 3) {
                                storeBuffer[storeLength] = readBuf[i];
                                storeLength = 0;
                                //Log.i(TAG, "Read from MCU: " + BinHex.byteArrayToHex(storeBuffer, 4));
                            }
                            break;

                        } else {
                            storeLength++;
                            if (storeLength < storeBuffer.length)
                                storeBuffer[storeLength] = readBuf[i];
                        }
                    }

                    break;
                case MESSAGE_WRITE:
                    broadcast(MESSAGE_WRITE);
                    break;

                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name

                    DeviceState.get().setDeviceName(msg.getData().getString(BluetoothService.DEVICE_NAME));
                    DeviceState.get().setDeviceMac(msg.getData().getString(BluetoothService.DEVICE_MAC));
                    broadcast(MESSAGE_DEVICE_NAME);
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
            }

        }

    };

    // Location service
    public static LocationManager mLocationManager;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    public static boolean isListening = false;
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    private Timer getLastLocationTimer;
    private final int MaxFilterCount = 6;
    private Location mPrelocation;
    private int mFilterCount;
    private Location mCurrentBestLocation;
    private long mLastLocationMillis;
    private Location preLocation;
    private long preLocationTime;

    // Cmds
    byte[] sendRequestDataUpdateCmd = new byte[]{(byte) 0xAA, 0x00, 0x0D};
    byte[] sendTimeAlignCmd;


    public Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (strMac.length() != 0) {
                Looper.prepare();

                // Connect to btDevice
                mBtService = new BluetoothService(BluetoothServiceManager.this, mHandler);
                mBtService.start();
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(strMac);
                mBtService.connect(device, false);

                //register phone call
                registerPhoneCall();


                // Thread loops
                if (D) Log.i(TAG, "Thread running");
                while (running) {
                    try {
                        Thread.sleep(100);
                        requestDataUpdate();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (D) Log.i(TAG, "Thread end");
            }
        }
    };


    private BroadcastReceiver phoneCallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "get phone call broadcast");
            if (intent.getBooleanExtra(PhoneCallReceiver.EXTRA_BROADCAST_RINGING, true)) {
                // incoming phone call
                sentBtPhoneRinging(intent.getStringExtra(PhoneCallReceiver.EXTRA_BROADCAST_NUM), true);
            } else {
                // end phone call
                sentBtPhoneRinging(intent.getStringExtra(PhoneCallReceiver.EXTRA_BROADCAST_NUM), false);
            }

        }
    };

    public static class BroadcastEvents {
        // Defines a custom Intent action
        public static final String BROADCAST_ACTION =
                "com.kdanmobile.kosometer.BROADCAST";

        // Defines the key for the status "extra" in an Intent
        public static final String EXTRA_STATUS =
                "com.kdanmobile.kosometer.STATUS";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }

    public void registerPhoneCall() {
        IntentFilter itFilter = new IntentFilter(PhoneCallReceiver.ACTION_BROADCAST_PHONECALL);
        LocalBroadcastManager.getInstance(this).registerReceiver(phoneCallReceiver, itFilter);
    }

    private void sentBtPhoneRinging(String phonenum, boolean ringing) {

        Log.i(TAG, "Phone is ringing");
        if (mBtService != null && mBtService.getState() == BluetoothService.STATE_CONNECTED) {
            phonenum.replace(" ", "");
            phonenum.trim();
            try {
                long num = 0;
                if(phonenum.length()>0) {
                    num = Long.valueOf(phonenum);
                }

                byte[] numByte = BCD.DecToBCDArray(num);
                byte[] length = BCD.DecToBCDArray(numByte.length);
                byte[] sendByte = new byte[20];
                sendByte[0] = (byte) 0xDD;
                sendByte[1] = 0x00;
//                sendByte[2] = length[0];
                sendByte[2] = 0x07;

                if (ringing) {
                    sendByte[3] = 0x02;
                    sendByte[4] = 0x55;
                } else {
                    sendByte[3] = 0x00;
                    sendByte[4] = 0x00;
                }

                for (int i = 0; i < numByte.length; i++) {
                    sendByte[i + 5] = numByte[i];
                }

                String strHex;
                byte[] crcHiLo = new byte[2];
                int crc;

                crc = DataCheck.crc16(numByte);
                strHex = Integer.toHexString(crc).toUpperCase();
                crcHiLo = BinHex.hexStringToByteArray(strHex);
                sendByte[17] = crcHiLo[0]; // crc high
                sendByte[18] = crcHiLo[1]; // crc low
                sendByte[19] = 0x0D;
                Log.i(TAG, "Write to bt phone ringing: " + BinHex.byteArrayToHex(sendByte, sendByte.length));
                mBtService.write(sendByte);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (D) Log.i(TAG, "onStartCommand()");

        if (!intent.hasExtra(EXTRA_ACTION)) {
            return -1;
        }
        String action = intent.getStringExtra(EXTRA_ACTION);

        if (action.equals(ACTION_START)) {

            strMac = getMacAddr(intent);
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (checkBlueTooth() && strMac.length() > 0) {
                if (!running || mThread == null) {
                    running = true;
                    mThread = new Thread(mRunnable);
                    mThread.start();
                }
            }
        } else if (action.equals(ACTION_STOP)) {
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void requestDataUpdate() {
        if (mBtService != null && mBtService.getState() == BluetoothService.STATE_CONNECTED) {
            try {
                //Log.i(TAG, "Write to bt: " + BinHex.byteArrayToHex(sendRequestDataUpdateCmd, sendRequestDataUpdateCmd.length));
                mBtService.write(sendRequestDataUpdateCmd);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void requestTimeAlign(){
        if (mBtService != null && mBtService.getState() == BluetoothService.STATE_CONNECTED) {
            try {
                Calendar calendar = Calendar.getInstance();
                sendTimeAlignCmd = new byte[20];
                sendTimeAlignCmd[0] = (byte)0xDD;
                sendTimeAlignCmd[1] = 0x01;
                sendTimeAlignCmd[2] = 0x08;
                byte[] yy = BCD.DecToBCDArray(calendar.get(Calendar.YEAR));
                byte[] m = BCD.DecToBCDArray(calendar.get(Calendar.MONTH)+1);
                byte[] d = BCD.DecToBCDArray(calendar.get(Calendar.DAY_OF_MONTH));
                byte[] h = BCD.DecToBCDArray(calendar.get(Calendar.HOUR_OF_DAY));
                byte[] min = BCD.DecToBCDArray(calendar.get(Calendar.MINUTE));
                byte[] s = BCD.DecToBCDArray(calendar.get(Calendar.SECOND));

                sendTimeAlignCmd[3] = yy[0];
                sendTimeAlignCmd[4] = yy[1];
                sendTimeAlignCmd[5] = m[0];
                sendTimeAlignCmd[6] = d[0];
                sendTimeAlignCmd[7] = 0x07;  // <--- what is this?
                sendTimeAlignCmd[8] = h[0];
                sendTimeAlignCmd[9] = min[0];
                sendTimeAlignCmd[10] = s[0];
                sendTimeAlignCmd[17] = 0x00;
                sendTimeAlignCmd[18] = 0x00;
                sendTimeAlignCmd[19] = 0x0D;

//                Log.i(TAG, "Write to bt [Time align]: " + BinHex.byteArrayToHex(sendTimeAlignCmd, sendTimeAlignCmd.length));
                mBtService.write(sendTimeAlignCmd);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void removeLocationUpdate() {
        if (isListening) {
            mLocationManager.removeUpdates(BluetoothServiceManager.this);
            isListening = false;
        }
    }

    private void requestLocationUpdates() {
        try {

            // exceptions will be thrown if provider is not permitted.
            try {
                gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }
            try {
                network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            if (isListening == false) {

                // don't start listeners if no provider is enabled
                if (!gps_enabled && !network_enabled)
                    return;

                if (gps_enabled)
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                if (network_enabled)
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

                isListening = true;
            }
            getLastLocationTimer = new Timer();
            getLastLocationTimer.schedule(new GetLastLocation(), 500);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getMacAddr(Intent it) {
        if (it.hasExtra(EXTRA_MAC)) {
            return it.getStringExtra(EXTRA_MAC);
        } else {
            return "";
        }
    }

    private boolean checkBlueTooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableIntent);
            return false;
        } else {
            return true;
        }
    }

    private void showForgroundNotification() {

        // Stop intent when disconnect on notification pressed
        Intent stopIntent = new Intent(this, BluetoothServiceManager.class);
        stopIntent.setAction(Long.toString(System.currentTimeMillis()));
        stopIntent.putExtra(EXTRA_ACTION, ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Start activity intent when notification pressed
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .addAction(R.drawable.ic_stat_stop, getString(R.string.disconnect), stopPendingIntent)
                        .setTicker(getString(R.string.device_connected))
                        .setSmallIcon(R.drawable.ic_stat_notify)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.device_connected))
                        .setContentIntent(pendingIntent);


        startForeground(ONGOING_NOTIFICATION_ID, mBuilder.build());
    }

    public static void startService(Context context, String mac) {
        Intent it = new Intent(context, BluetoothServiceManager.class);
        it.putExtra(EXTRA_ACTION, ACTION_START);
        it.putExtra(EXTRA_MAC, mac);
        context.startService(it);
    }

    public static void stopService(Context context) {
        Intent it = new Intent(context, BluetoothServiceManager.class);
        it.putExtra(EXTRA_ACTION, ACTION_STOP);

        context.stopService(it);
    }


    public void broadcast(int type) {
        Intent it = new Intent(ACTION_BROADCAST_DATA);
        it.putExtra(EXTRA_BROADCAST_MESSAGE, type);
        LocalBroadcastManager.getInstance(this).sendBroadcast(it);
    }

    public void broadcast(int type, String data) {
        Intent it = new Intent(ACTION_BROADCAST_DATA);
        it.putExtra(EXTRA_BROADCAST_MESSAGE, type);
        it.putExtra(EXTRA_BROADCAST_DATA, data);
        LocalBroadcastManager.getInstance(this).sendBroadcast(it);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (D) Log.i(TAG, "onDestroy()");

        running = false;
        try {
            // Stop data recording
            DataRecorder.get().stop(this);
            removeLocationUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        stopForeground(true);
        mBtService.stop();
        broadcast(MESSAGE_STATE_CHANGE);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(phoneCallReceiver);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (getLastLocationTimer != null)
            getLastLocationTimer.cancel();

        // filter the dither location
        if (mPrelocation != null && location.distanceTo(mPrelocation) < location.getAccuracy() && mFilterCount < MaxFilterCount) {
            mFilterCount++;
            return;
        } else {
            mFilterCount = 0;
            mPrelocation = location;
        }

        if (isBetterLocation(location, mCurrentBestLocation)) {

            //broadcast the location
            if (preLocation != null) {
                double deltaTime = (location.getTime() - preLocation.getTime()) / 3600000d;
                double distance = location.distanceTo(preLocation) / 1000d;

                location.setSpeed((float) (distance / deltaTime));
            }

            broadcast(MESSAGE_ONLOCATION_CHANGE, location.getLatitude() + "," + location.getLongitude());
            DataRecorder.get().addData(BluetoothServiceManager.this,RecordManager.DATA_LOCATION, location.getLatitude() + "," + location.getLongitude());

            mCurrentBestLocation = location;
            mLastLocationMillis = SystemClock.elapsedRealtime();
            preLocation = location;

            saveLocationToPreferenceManager();
        } else {
            // just update location
            // mRoute.setCurrentLocation(new
            // Latlng(location.getLatitude(),location.getLongitude()));
            // mRoute.invalidate();
        }
    }

    private void saveLocationToPreferenceManager(){
        PreferenceManager.get(this).setLASTEST_LOCATION(mCurrentBestLocation.getLatitude() + "," + mCurrentBestLocation.getLongitude() + "," + System.currentTimeMillis());
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    // Checks whether two providers are the same
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        broadcast(MESSAGE_ONLOCATION_CHANGE);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {

            Location net_loc = null, gps_loc = null;
            if (gps_enabled)
                gps_loc = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (network_enabled)
                net_loc = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            // if there are both values use the latest one
            if (gps_loc != null && net_loc != null) {
                if (gps_loc.getTime() > net_loc.getTime()) {

                    broadcast(MESSAGE_ONLOCATION_CHANGE);
                } else {
                    broadcast(MESSAGE_ONLOCATION_CHANGE);
                }
                return;
            }

            if (gps_loc != null) {
                broadcast(MESSAGE_ONLOCATION_CHANGE);
                return;
            }
            if (net_loc != null) {
                broadcast(MESSAGE_ONLOCATION_CHANGE);
                return;
            }

        }
    }
}
