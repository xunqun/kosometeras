package com.kdanmobile.kosometer.fingerchart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.support.v4.view.MotionEventCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.kdanmobile.kosometer.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by snippets on 14/12/8.
 */

public class ChartView extends View {


    private final String TAG = "ChartView";
    private Context mContext;

    List<ChartPoint> mChartPoints = new ArrayList<ChartPoint>();

    // Custom style
    private int mLineColor = 0xFF808080;
    private int mFrameColor = 0xFFD0D0D0;
    private int mBgColor = 0xFFF6F6F6;

    // Properties
    private double maxX = 0;
    private double minX = 0;
    private double maxY = 0;
    private double minY = 0;

    // Path & Paint
    Paint mSelectedPointPaint = new Paint();
    Paint mPaint = new Paint();
    Paint mFramePaint = new Paint();
    TextPaint mTextPaint = new TextPaint();
    int textSize = 30;

    Path mSelectedPath = new Path();
    Path mPath = null;
    Path mFramePath = null;
    Path mTopTextPath = new Path();
    Path mMidTextPath = new Path();
    Path mBottomTextPath = new Path();


    //
    OnSelectedXListener listener;
    ChartPoint mSelectedPoint;
    private boolean isMoving = false;

    public interface OnSelectedXListener {
        void onSelected(double x, ChartPoint point);

        void onActionUp();
    }

    public ChartView(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        getCustomStyle(attrs);
        init();
    }

    public void setOnSelectedXListener(OnSelectedXListener l) {
        listener = l;
    }

    private ChartPoint getApproximateX(double d) {
        try {
            int position = binarySearch(0, mChartPoints.size() - 1, d);
            return mChartPoints.get(position);
        }catch (Exception e){
            throw new NullPointerException();
        }

    }

    private int binarySearch(int from, int to, double target) {
        int index = Math.round((from + to) / 2);
        if (index == from || index == to) {
            return index;
        }

        if (mChartPoints.get(index).x == target) {
            return index;
        } else if (mChartPoints.get(index).x > target) {
            return binarySearch(from, index, target);
        } else {
            return binarySearch(index, to, target);
        }
    }


    private void initPathNPaint() {
        mPaint.setColor(mLineColor);
        mPaint.setStrokeWidth(10);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setAntiAlias(true);

        mSelectedPointPaint.setColor(Color.BLUE);
        mSelectedPointPaint.setStyle(Paint.Style.FILL);
        mSelectedPointPaint.setAntiAlias(true);

        mFramePaint.setColor(Color.GRAY);
        mFramePaint.setStrokeWidth(5);
        mFramePaint.setStyle(Paint.Style.STROKE);

        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextSize(textSize);

    }

    private void init() {
        initPathNPaint();

    }

    private void sortList() {
        Collections.sort(mChartPoints, new Comparator<ChartPoint>() {
            @Override
            public int compare(ChartPoint lhs, ChartPoint rhs) {
                if (lhs.x < rhs.x) return -1;
                if (lhs.x == rhs.x) return 0;
                return 1;
            }
        });
    }

    private void getCustomStyle(AttributeSet attrs) {
        TypedArray a = mContext.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.Chart,
                0, 0);
        try {
            mLineColor = a.getInteger(R.styleable.Chart_lineColor, mLineColor);
            mFrameColor = a.getInteger(R.styleable.Chart_frameColor, mFrameColor);
            mBgColor = a.getInteger(R.styleable.Chart_bgColor, mBgColor);
        } finally {
            a.recycle();
        }
    }

    public void setChartPoints(ArrayList<ChartPoint> points) {
        if(points == null || points.size() ==0){
            return;
        }

        mChartPoints = points;

        // init maxX, Y and minX, Y
        ChartPoint point = mChartPoints.get(0);
        maxX = minX = point.x;
        maxY = minY = point.y;
        for (int i = 1; i < mChartPoints.size(); i++) {
            point = mChartPoints.get(i);
            setMinMaxProperties(point);
        }
        sortList();
        mPath = null;

        if(minY == maxY){
            maxY = maxY + 10;
            minY = minY - 10;
        }else {
            double span = maxY - minY;
            maxY = maxY + span * 0.2;
            minY = minY - span * 0.2;
        }
        invalidate();
    }

    public void addChartPoint(ChartPoint point) {
        setMinMaxProperties(point);
        sortList();
        mPath = null;
        invalidate();
    }

    private void setMinMaxProperties(ChartPoint point) {
        if (point.x > maxX)
            maxX = point.x;
        if (point.x < minX)
            minX = point.x;
        if (point.y > maxY)
            maxY = point.y;
        if (point.y < minY)
            minY = point.y;
    }

    private void genLinePath() {
        PointF screenPoint;
        mPath = new Path();
        if (mChartPoints.size() > 1) {
            for (int i = 0; i < mChartPoints.size(); i++) {
                ChartPoint p = mChartPoints.get(i);
                screenPoint = dataToScreen(p);
                if (i == 0) {
                    mPath.moveTo(screenPoint.x, screenPoint.y);
                } else {
                    mPath.lineTo(screenPoint.x, screenPoint.y);
                }
            }
        }
    }

    private PointF dataToScreen(ChartPoint p) {

        float screenX = (float) ((p.x - minX) / (maxX - minX)) * getWidth();
        float screenY = getHeight() - (float) ((p.y - minY) / (maxY - minY)) * getHeight();
        return new PointF(screenX, screenY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case (MotionEvent.ACTION_DOWN):

                return true;
            case (MotionEvent.ACTION_MOVE):
                isMoving = true;
                handleTouch(event.getX());
                return true;

            case (MotionEvent.ACTION_UP):
                isMoving = false;
                handleActionUp();
                return true;
            default:
                return super.onTouchEvent(event);
        }

    }

    private void handleActionUp() {
        listener.onActionUp();
        invalidate();
    }

    private void handleTouch(float x) {

        try {
            double selectedValueX = (x / getWidth()) * (maxX - minX) + minX;
            mSelectedPoint = getApproximateX(selectedValueX);

            // Notify the position x has been selected
            Log.i(TAG, "selected value:" + selectedValueX);
            if (listener != null)
                listener.onSelected(selectedValueX, mSelectedPoint);
            invalidate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (getWidth() != 0 && getHeight() != 0) {
            if (mPath == null) {
                genLinePath();
            }
            canvas.drawColor(mBgColor);
            canvas.drawPath(mPath, mPaint);

            if (mSelectedPoint != null && isMoving) {
                drawSelectedPoint(canvas, mSelectedPoint);
            }

            drawFrame(canvas, mPaint);
        }
    }

    private void drawFrame(Canvas canvas, Paint paint) {
        if (getHeight() <= 0)
            return;

        if (mFramePath == null) {
            mFramePath = new Path();
            mFramePath.moveTo(0, 0);
            mFramePath.lineTo(0, getHeight());
            mFramePath.lineTo(getWidth(), getHeight());

            mFramePath.moveTo(0, 0);
            mFramePath.lineTo(20, 0);
            mFramePath.moveTo(0, getHeight() / 2);
            mFramePath.lineTo(20, getHeight() / 2);

            mTopTextPath.moveTo(0, textSize);
            mTopTextPath.lineTo(getWidth(), textSize);
            mMidTextPath.moveTo(0, getHeight() / 2 + textSize);
            mMidTextPath.lineTo(getWidth(), getHeight() / 2 + textSize);
            mBottomTextPath.moveTo(0, getHeight());
            mBottomTextPath.lineTo(getWidth(), getHeight());
        }
        canvas.drawPath(mFramePath, mFramePaint);
        canvas.drawTextOnPath(String.valueOf(maxY), mTopTextPath, 10, 10, mTextPaint);
        canvas.drawTextOnPath(String.valueOf((maxY + minY) / 2), mMidTextPath, 10, 10, mTextPaint);
        canvas.drawTextOnPath(String.valueOf(minY), mBottomTextPath, 10, -10, mTextPaint);
    }

    private void drawSelectedPoint(Canvas canvas, ChartPoint point) {
        PointF p = dataToScreen(point);
        mSelectedPath = new Path();
        mSelectedPath.addCircle(p.x, p.y, 10, Path.Direction.CCW);
        canvas.drawPath(mSelectedPath, mSelectedPointPaint);

    }

    public static class ChartPoint {
        public ChartPoint(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double x;
        public double y;
    }
}
