package com.kdanmobile.kosometer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by snippets on 14/11/28.
 */
public class PreferenceManager {
    private static PreferenceManager instance;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static Context mContext;
    private static String FILE_NAME = "kosometer.pref";

    public PreferenceManager(Context context) {
        mContext = context;
        mSharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public static PreferenceManager get(Context context) {
        if (instance == null || mContext != context) {
            instance = new PreferenceManager(context);
        }
        return instance;
    }

    private String PREVIOUS_MAC_ADDR = "SAVED_MAC_ADDR";

    public String getPREVIOUS_MAC_ADDR() {
        return mSharedPreferences.getString(PREVIOUS_MAC_ADDR, "");
    }

    public void setPREVIOUS_MAC_ADDR(String s) {
        mEditor.putString(PREVIOUS_MAC_ADDR, s);
        mEditor.commit();
    }

    private String LASTEST_LOCATION = "LASTEST_LOCATION";

    public String getLASTEST_LOCATION(){
        return mSharedPreferences.getString(LASTEST_LOCATION, "");
    }

    public void setLASTEST_LOCATION(String s){
        mEditor.putString(LASTEST_LOCATION, s);
        mEditor.commit();
    }


}
