package com.kdanmobile.kosometer.utils;

import java.util.Calendar;

/**
 * Created by snippets on 15/2/6.
 */
public class StringUtils {
    public static String getDisplayTimeString(long timestamp){
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR) + "-" +
                cal.get(Calendar.MONTH)+1 + "-" +
                cal.get(Calendar.DAY_OF_MONTH) + " " +
                cal.get(Calendar.HOUR_OF_DAY) + ":" +
                cal.get(Calendar.MINUTE) + ":" +
                cal.get(Calendar.SECOND);
    }
}
