package com.kdanmobile.kosometer.utils;

public class DataCheck {

    /**
     * 可
     *
     * @param buffer
     * @return
     */
    public static int crc16(byte[] buffer) {
        int crc = 0xFFFF;

        for (int j = 0; j < buffer.length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;

    }
//	/**
//	 * 可
//	 * @param bytes
//	 * @param length
//	 * @return
//	 */
//    public static int getCrc16CheckSum(byte bytes[], int length){
//        int crc = 0xFFFF;
//        int temp;
//        int crc_byte;
//        for (int byte_index = 0; byte_index < bytes.length; byte_index++) {
//            crc_byte = bytes[byte_index];
//            for (int bit_index = 0; bit_index < 8; bit_index++) {
//                temp = ((crc >> 15)) ^ ((crc_byte >> 7));
//                crc <<= 1;
//                crc &= 0xFFFF;
//                if (temp > 0) {
//                    crc ^= 0x1021;
//                    crc &= 0xFFFF;
//                }
//                crc_byte <<=1;
//                crc_byte &= 0xFF;
//            }
//        }
//        return crc;
//    }
}
