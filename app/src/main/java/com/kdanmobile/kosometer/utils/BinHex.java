package com.kdanmobile.kosometer.utils;

import java.util.regex.Pattern;

public class BinHex {
    // HEX不分大小寫
    private final static Pattern HEX_PATTERN = Pattern.compile("^[0-9A-Fa-f]+$");
    private final static Pattern MACADDR_PATTERN = Pattern.compile("^[a-fA-F0-9]{12}$");


    public static boolean isHexValueValid(String hexval) {
        return HEX_PATTERN.matcher(hexval).matches();
    }

    public static boolean isMacAddrValid(String macAddr) {
        return MACADDR_PATTERN.matcher(macAddr).matches();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static byte hexStringToByte(String s) {
        int len = s.length();
        byte data = 0;
        for (int i = 0; i < len; i += 2) {
            data = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String byteArrayToHex(byte[] a, int start, int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append(String.format("%02X", a[i + start] & 0xff));
        }
        return sb.toString();
    }

    public static String byteArrayToHex(byte[] a, int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append(String.format("%02X ", a[i] & 0xff));
        }
        return sb.toString();
    }
}

/*****************************************************************************
 PATTERN字元表示意義
 ^            Start of line.
 [0-9A-F]     Character class: Any character in 0 to 9, or in A to F.
 +            Quantifier: One or more of the above.
 $            End of line.
 *****************************************************************************/


