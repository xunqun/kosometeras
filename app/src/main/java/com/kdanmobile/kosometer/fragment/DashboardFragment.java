package com.kdanmobile.kosometer.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.activity.DeviceListActivity;
import com.kdanmobile.kosometer.bluetooth.BluetoothService;
import com.kdanmobile.kosometer.bluetooth.BluetoothServiceManager;
import com.kdanmobile.kosometer.bluetooth.DeviceState;
import com.kdanmobile.kosometer.model.IncomingDataAdapter;
import com.kdanmobile.kosometer.utils.PreferenceManager;
import com.kdanmobile.kosometer.views.RoundMeter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends BaseFragment {

    public final String TAG = "DashboardFragment";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;


    private RelativeLayout mSearchFrame;
    private Button mSearchButton;
    private GridView mGridView;
    private GridViewAdapter mGridViewAdapter;

    private MenuItem mToggleBtn;

    // Instance value
    private String mInstanceSpeed = "0";
    private String mInstanceRpm = "0";
    private String mInstanceMileage = "0";
    private String mInstanceTotalMileage = "0";
    private String mInstanceTemprature = "0";
    private String mInstanceAfRatio = "0";
    private String mInstanceVoltage = "0";


    // Instance Data
    private final int DATA_SPEED = 0;
    private final int DATA_RPM = 1;
    private final int DATA_MILEAGE = 2;
    private final int DATA_TOTAL_MILEAGE = 3;
    private final int DATA_TEMPRATURE = 4;
    private final int DATA_AF_RATIO = 5;
    private final int DATA_VOLTAGE = 6;

    private boolean isDoAutoConnect = false;

    // Bluetooth broadcast receiver
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int action = intent.getIntExtra(BluetoothServiceManager.EXTRA_BROADCAST_MESSAGE, -1);
            switch (action) {
                case BluetoothServiceManager.MESSAGE_STATE_CHANGE:
                    handleMessageStateChange();
                    break;

                case BluetoothServiceManager.MESSAGE_DEVICE_NAME:
                    handleDeviceName();
                    break;

                case BluetoothServiceManager.MESSAGE_READ:
                    handleRead();
                    break;

                case BluetoothServiceManager.MESSAGE_TOAST:
                    handleToast();
                    break;

                case BluetoothServiceManager.MESSAGE_WRITE:
                    handleWrite();
                    break;

                case BluetoothServiceManager.MESSAGE_ONLOCATION_CHANGE:
                    if (intent.hasExtra(BluetoothServiceManager.EXTRA_BROADCAST_DATA)) {
                        String loc = intent.getStringExtra(BluetoothServiceManager.EXTRA_BROADCAST_DATA);
                        String[] latlng = loc.split(",");
                        handleLocationChange(new LatLng(Double.valueOf(latlng[0]), Double.valueOf(latlng[1])));
                    }
                    break;

            }
        }
    };


    private void handleLocationChange(LatLng location) {

    }

    private void handleMessageStateChange() {
        if (DeviceState.get().getConnectState() == BluetoothService.STATE_CONNECTED) {
            showProgress(true);
            showSearchFrame(false);
        } else if (DeviceState.get().getConnectState() == BluetoothService.STATE_CONNECTING) {
            showSearchFrame(true);
            showProgress(true);
        } else {
            showSearchFrame(true);
            showProgress(false);
        }
        getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
    }

    private void handleDeviceName() {
        PreferenceManager.get(getActivity()).setPREVIOUS_MAC_ADDR(DeviceState.get().getDeviceMac());
    }

    private void handleRead() {
        byte[] buffer = DeviceState.get().getIncomingBuffer().clone();

        // incoming instance data
//        Log.i(TAG, String.valueOf("get command: " + buffer[0]));
        if (buffer[0] == -86) {  // -86 --> 0xAA
            IncomingDataAdapter dataset = new IncomingDataAdapter(buffer);
            mInstanceSpeed = dataset.getSpeed();
            mInstanceRpm = dataset.getRpm();
            mInstanceAfRatio = dataset.getAirFuelRatio();
            mInstanceMileage = dataset.getMileage();
            mInstanceTotalMileage = dataset.getTotalMileage();
            mInstanceVoltage = dataset.getVoltage();
            mInstanceTemprature = dataset.getTemprature();
            mGridViewAdapter.notifyDataSetChanged();
        }

    }

    private void handleToast() {

    }

    private void handleWrite() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public DashboardFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

    }



    @Override
    public void onStart() {
        super.onStart();
        if(!isDoAutoConnect) {
            checkAndAutoConnect();
            isDoAutoConnect = true;
        }
    }

    private void checkAndAutoConnect(){
        SharedPreferences sharedPref = android.preference.PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean autoconnect = sharedPref.getBoolean("auto_connect_when_start",false);
        if (DeviceState.get().getConnectState() == BluetoothService.STATE_NONE && autoconnect)
            tryToConnectPreviousDevice();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                freshToggleButton();

                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                freshToggleButton();

                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    chooseDevice();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Toast.makeText(getActivity(), R.string.bt_not_enabled, Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        connectDevice(address);
    }

    private void connectDevice(String macAddress) {
        BluetoothServiceManager.startService(getActivity(), macAddress);
    }

    ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        initViews();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.dashboard, menu);
        mToggleBtn = menu.findItem(R.id.action_switch);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mToggleBtn = menu.findItem(R.id.action_switch);
        freshToggleButton();
    }

    private void freshToggleButton() {
        if (DeviceState.get().getConnectState() == BluetoothService.STATE_CONNECTED) {
            mToggleBtn.setIcon(R.drawable.ic_action_stop);

        } else {
            mToggleBtn.setIcon(R.drawable.ic_action_connect);
        }
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_switch) {
            if (DeviceState.get().getConnectState() == BluetoothService.STATE_CONNECTED) {
                BluetoothServiceManager.stopService(getActivity());
            } else {
                chooseDevice();
            }
            freshToggleButton();
            return true;
        }

        return false;
    }

    private void initViews() {
        mSearchFrame = (RelativeLayout) getView().findViewById(R.id.search_frame);
        mSearchButton = (Button) getView().findViewById(R.id.search_button);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDevice();
            }
        });

        mGridView = (GridView) getView().findViewById(R.id.gridview);
        mGridViewAdapter = new GridViewAdapter();
        mGridView.setAdapter(mGridViewAdapter);
    }

    private void chooseDevice() {

        // Check point 2 : Setting up bluetooth

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return;
        }

        // Show device list for choice
        Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
    }

    private void showSearchFrame(boolean show) {
        if (show) {

            if (mSearchFrame.getVisibility() != View.VISIBLE) {
                mSearchFrame.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
                mSearchFrame.setVisibility(View.VISIBLE);
            }
        } else {

            if (mSearchFrame.getVisibility() != View.GONE) {
                mSearchFrame.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out));
                mSearchFrame.setVisibility(View.GONE);
            }
        }
    }

    private void showProgress(boolean show) {
        if (show) {
            getView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        } else {
            getView().findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mOnFragmentInteractionCallback != null)
            mOnFragmentInteractionCallback.onAttached();

    }

    @Override
    public void onResume() {
        super.onResume();
        subscibeBluetoohEvent();
        if (DeviceState.get().getConnectState() == BluetoothService.STATE_CONNECTED) {
            showSearchFrame(false);
        } else {
            showSearchFrame(true);
        }
    }

    public void tryToConnectPreviousDevice() {

        String savedMac = PreferenceManager.get(getActivity()).getPREVIOUS_MAC_ADDR();
        if (savedMac.length() > 0) {
            connectDevice(savedMac);
            showSearchFrame(true);
            showProgress(true);

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        unsubscibeBluetoohEvent();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void subscibeBluetoohEvent() {

        IntentFilter filter = new IntentFilter(BluetoothServiceManager.ACTION_BROADCAST_DATA);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, filter);
    }

    private void unsubscibeBluetoohEvent() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
    }

    public class GridViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder= null;
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.frame_dashboard_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder)convertView.getTag();

            try {
                switch (position) {
                    case DATA_SPEED:
                        viewHolder.getTitle().setText(getString(R.string.speed));
                        viewHolder.getValue().setText(mInstanceSpeed);
                        viewHolder.getUnit().setText("kmh");
                        viewHolder.getMeter().setVisibility(View.VISIBLE);
                        viewHolder.getMeter().setParameters(220, 0, 240, 120);
                        viewHolder.getMeter().setCurrentValue(Integer.valueOf(mInstanceSpeed.length() == 0 ? "0" : mInstanceSpeed));
                        break;
                    case DATA_RPM:
                        viewHolder.getTitle().setText(getString(R.string.rpm));
                        viewHolder.getValue().setText(mInstanceRpm);
                        viewHolder.getMeter().setVisibility(View.VISIBLE);
                        viewHolder.getMeter().setParameters(15000, 0, 240, 120);
                        viewHolder.getMeter().setCurrentValue(Integer.valueOf(mInstanceRpm));
                        viewHolder.getUnit().setText("rpm");
                        break;

                    case DATA_MILEAGE:
                        viewHolder.getTitle().setText(getString(R.string.mileage));
                        viewHolder.getValue().setText(mInstanceMileage);
                        viewHolder.getMeter().setVisibility(View.GONE);
                        viewHolder.getUnit().setText("km");
                        break;

                    case DATA_TOTAL_MILEAGE:
                        viewHolder.getTitle().setText(getString(R.string.total_mileage));
                        viewHolder.getValue().setText(mInstanceTotalMileage);
                        viewHolder.getMeter().setVisibility(View.GONE);
                        viewHolder.getUnit().setText("km");
                        break;
                    case DATA_TEMPRATURE:
                        viewHolder.getTitle().setText(getString(R.string.temprature));
                        viewHolder.getValue().setText(mInstanceTemprature);
                        viewHolder.getMeter().setVisibility(View.GONE);
                        viewHolder.getUnit().setText("c");
                        break;
                    case DATA_AF_RATIO:
                        viewHolder.getTitle().setText(getString(R.string.af_ratio));
                        viewHolder.getValue().setText(mInstanceAfRatio);
                        viewHolder.getMeter().setVisibility(View.GONE);
                        viewHolder.getUnit().setText("");
                        break;
                    case DATA_VOLTAGE:
                        viewHolder.getTitle().setText(getString(R.string.voltage));
                        viewHolder.getValue().setText(mInstanceVoltage);
                        viewHolder.getMeter().setVisibility(View.GONE);
                        viewHolder.getUnit().setText("v");
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }
    }

    private class ViewHolder {
        TextView title;
        TextView value;
        TextView unit;
        RoundMeter meter;

        public ViewHolder(View convertView) {
            title = (TextView) convertView.findViewById(R.id.title);
            value = (TextView) convertView.findViewById(R.id.value);
            unit = (TextView) convertView.findViewById(R.id.unit);
            meter = (RoundMeter) convertView.findViewById(R.id.meter);
        }

        public TextView getTitle() {
            return title;
        }

        public TextView getValue() {
            return value;
        }

        public TextView getUnit() {
            return unit;
        }

        public RoundMeter getMeter() {
            return meter;
        }
    }


}
