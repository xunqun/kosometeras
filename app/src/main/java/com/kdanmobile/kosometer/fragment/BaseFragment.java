package com.kdanmobile.kosometer.fragment;


import android.support.v4.app.Fragment;

import com.kdanmobile.kosometer.activity.MainActivity;


public class BaseFragment extends Fragment {
    protected MainActivity.OnFragmentInteractionCallback mOnFragmentInteractionCallback;

    public void setOnFragmentInteractionCallback(MainActivity.OnFragmentInteractionCallback callback) {
        mOnFragmentInteractionCallback = callback;
    }

}
