package com.kdanmobile.kosometer.fragment;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.utils.PreferenceManager;
import com.kdanmobile.kosometer.utils.StringUtils;

/**
 * Created by snippets on 15/2/6.
 */
public class LastLocationFragment extends BaseFragment implements OnMapReadyCallback {

    TextView mTime;
    SupportMapFragment mMapFragment;
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private Location mMylocation;
    private LatLng mLastSavedLocation;
    private GoogleMap mGoogleMap;
    private LinearLayout vSavedLocationButton;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_last_location_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        mMapFragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_wrapper, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync((OnMapReadyCallback) this);

        vSavedLocationButton = (LinearLayout)getView().findViewById(R.id.btn);
        vSavedLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLastSavedLocation != null) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLastSavedLocation, 20));
                }
            }
        });

    }

    public static LastLocationFragment newInstance(String param1) {
        LastLocationFragment fragment = new LastLocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {

        FragmentManager fm = getFragmentManager();

        Fragment xmlFragment = fm.findFragmentById(R.id.map);
        if (xmlFragment != null) {
            fm.beginTransaction().remove(xmlFragment).commit();
        }

        super.onDestroyView();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mGoogleMap = googleMap;
        initMap(googleMap);

        String lastLocation = PreferenceManager.get(getActivity()).getLASTEST_LOCATION();
        if (lastLocation.length() > 0) {
            String[] locationArray = lastLocation.split(",");
            long lasttimestamp = Long.valueOf(locationArray[2]);
            mTime = (TextView) getView().findViewById(R.id.time);
            mTime.setText(StringUtils.getDisplayTimeString(lasttimestamp));
            mLastSavedLocation = new LatLng(Double.valueOf(locationArray[0]), Double.valueOf(locationArray[1]));
            addMarkerAndPanToLocation(googleMap, mLastSavedLocation);
        } else {
            mTime = (TextView) getView().findViewById(R.id.time);
            mTime.setText(R.string.no_saved_last_location);
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    if (mMylocation == null)
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 20));

                    mMylocation = location;
                }
            });
        }
    }

    private void initMap(GoogleMap map) {
        map.setMyLocationEnabled(true);
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if(vSavedLocationButton.getVisibility() == View.GONE) {
                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button_in);
                    vSavedLocationButton.startAnimation(anim);
                    vSavedLocationButton.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void addMarkerAndPanToLocation(GoogleMap map, LatLng latlng) {
        map.addMarker(new MarkerOptions()
                .position(latlng)
                .title(getString(R.string.title_section_last_location)));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 20));

    }


}
