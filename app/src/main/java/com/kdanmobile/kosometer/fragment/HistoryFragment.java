package com.kdanmobile.kosometer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.activity.RecordActivity;
import com.kdanmobile.kosometer.model.RecordManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends BaseFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    // Views
    private GridView mGridView;
    private GridViewAdapter mGridViewAdapter;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    ArrayList<RecordManager.RecordSet> mRecordSets = new ArrayList<RecordManager.RecordSet>();
    RecordManager.OnRecordSetChangedListener mOnRecordSetChangedListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        initViews();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecordManager.recycle();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RecordManager.recycle();
    }

    private void initViews() {
        mGridView = (GridView) getView().findViewById(R.id.gridview);
        mGridViewAdapter = new GridViewAdapter();
        mGridView.setAdapter(mGridViewAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getActivity(), RecordActivity.class);
                it.putExtra(RecordActivity.EXTRA_POSITION, position);
                startActivity(it);
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDeleteDialog(position);
                return true;
            }
        });

        mProgressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        RecordManager.get(getActivity()).notifyDatasetChanged(getActivity());
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        mSwipeRefreshLayout .setRefreshing(false);
                    }
                }.execute();

            }
        });
        mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void showDeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_record);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RecordManager.get(getActivity()).deleteRecord(getActivity(), mRecordSets.get(pos).getId());
                mGridViewAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();

    }

    private void registerListeners() {
        mOnRecordSetChangedListener = new RecordManager.OnRecordSetChangedListener() {

            @Override
            public void onChanged() {
                // To retrieve all data set
                new AsyncTask<Void, Void, ArrayList>() {

                    @Override
                    protected void onPostExecute(ArrayList arrayList) {
                        super.onPostExecute(arrayList);
                        mRecordSets = arrayList;
                        mGridViewAdapter.notifyDataSetInvalidated();
                    }

                    @Override
                    protected ArrayList doInBackground(Void... params) {
                        ArrayList list = RecordManager.get(getActivity()).getRecordSets();
                        return list;
                    }
                }.execute();
            }
        };
        RecordManager.get(getActivity()).registerOnRecordSetChangedListener(mOnRecordSetChangedListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOnRecordSetChangedListener != null)
            RecordManager.get(getActivity()).unregisterOnRecordSetChangedListener(mOnRecordSetChangedListener);

    }

    @Override
    public void onResume() {
        super.onResume();
        // To retrieve all data set
        new AsyncTask<Void, Void, ArrayList>() {

            @Override
            protected void onPostExecute(ArrayList arrayList) {
                super.onPostExecute(arrayList);
                mRecordSets = arrayList;
                mGridViewAdapter.notifyDataSetInvalidated();
                registerListeners();
                if (mProgressBar.getVisibility() != View.GONE) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            protected ArrayList doInBackground(Void... params) {
                ArrayList list = RecordManager.get(getActivity()).getRecordSets();
                return list;
            }
        }.execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // To update the actionbar
        if (mOnFragmentInteractionCallback != null)
            mOnFragmentInteractionCallback.onAttached();
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_clear) {
            RecordManager.get(getActivity()).cleanAll(getActivity());
            return true;
        }

        return false;
    }

    private class GridViewAdapter extends BaseAdapter {
        String valueFormat = "%1$.2f";

        @Override
        public int getCount() {
            return mRecordSets.size();
        }

        @Override
        public RecordManager.RecordSet getItem(int position) {
            return mRecordSets.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            RecordManager.RecordSet set = mRecordSets.get(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.frame_history_item, null);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(set.getTimestamp());
            TextView time = (TextView) convertView.findViewById(R.id.time);
            TextView duration = (TextView) convertView.findViewById(R.id.duration);
            time.setText(calendar.get(Calendar.YEAR) + " - " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " - " + calendar.get(Calendar.DAY_OF_MONTH) + " " + calendar.get(Calendar.HOUR_OF_DAY) + " : " + calendar.get(Calendar.MINUTE));
            duration.setText(String.format(valueFormat, set.getDuration() / 3600000) + " hours");

            return convertView;
        }
    }
}
