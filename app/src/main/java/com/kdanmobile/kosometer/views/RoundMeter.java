package com.kdanmobile.kosometer.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by snippets on 15/1/5.
 */
public class RoundMeter extends View {
    String TAG = "RoundMeter";
    private float MAX_VALUE = 180;
    private float MIN_VALUE = 0;
    private float START_ANGLE = 240;
    private float END_ANGLE = 120 + 360;

    private Context context;
    private float mCurrentValue = 0;
    private float mAnimateValue = 0;
    private float mDiameter;

    // Path and paint
    private Paint bgPaint;
    private Paint scalePaint;
    private Path bgPath;
    private Path needlePath;
    private Path scalePath;
    ValueAnimator animator;

    public RoundMeter(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public RoundMeter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        initPaint();
    }

    public void setParameters(float maxValue, float minValue, float startAngle, float endAngle) {
        MAX_VALUE = maxValue;
        MIN_VALUE = minValue;
        START_ANGLE = startAngle;
        if (startAngle > endAngle) {
            END_ANGLE = endAngle + 360;
        } else {
            END_ANGLE = endAngle;
        }
    }

    private void initPaint() {
        bgPaint = new Paint();
        bgPaint.setColor(Color.GRAY);
        bgPaint.setStrokeWidth(3);
        bgPaint.setStyle(Paint.Style.STROKE);

        scalePaint = new Paint();
        scalePaint.setColor(Color.GRAY);
        scalePaint.setStrokeWidth(3);
        scalePaint.setStyle(Paint.Style.STROKE);

    }

    public void setCurrentValue(float value) {
        //for debug
        //value = value + (float) Math.random() * (MAX_VALUE / 2);

        mCurrentValue = validateValue(value);
        setAnimateValue();

    }

    public float validateValue(float value) {
        if (value > MAX_VALUE) {
           return MAX_VALUE;
        } else if (value < MIN_VALUE) {
            return MIN_VALUE;
        } else {
            return value;
        }
    }

    public void setAnimateValue() {
        if(mAnimateValue != mCurrentValue) {
            if(animator!=null && animator.isRunning()){
                animator.cancel();
                animator = null;
            }
            animator = new ValueAnimator().ofFloat(mAnimateValue, mCurrentValue);
            animator.setDuration(250);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    mAnimateValue = (float) animation.getAnimatedValue();
                    invalidate();
                }
            });
            animator.start();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        initLayoutProperties(w, h);
        initPath();
    }

    private void initLayoutProperties(int w, int h) {
        // Account for padding
        float xpad = (float) (getPaddingLeft() + getPaddingRight());
        float ypad = (float) (getPaddingTop() + getPaddingBottom());

        float ww = (float) w - xpad;
        float hh = (float) h - ypad;

        // Figure out how big we can make the pie.
        mDiameter = Math.min(ww, hh);
    }

    private void initPath() {
        bgPath = new Path();
        bgPath.addCircle(getWidth() / 2, getHeight() / 2, mDiameter / 2, Path.Direction.CCW);

        needlePath = new Path();
        needlePath.moveTo(getWidth() / 2, getHeight() / 2);
        needlePath.lineTo(getWidth() / 2, (getHeight() - mDiameter) / 2);

        scalePath = new Path();
        scalePath.moveTo(getWidth() / 2, getPaddingTop());
        scalePath.lineTo(getWidth() / 2, getPaddingTop() + (mDiameter / 20));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawNeedle(canvas);
        super.onDraw(canvas);
    }

    private void drawBackground(Canvas canvas) {

        drawRoundBg(canvas);
        //drawScaleEvery(canvas, 1, 1f);
        drawScaleEvery(canvas, (int) (MAX_VALUE - MIN_VALUE) / 10, 5f);
    }

    private void drawScaleEvery(Canvas canvas, int scale, float width) {
        scalePaint.setStrokeWidth(width);
        for (float i = MIN_VALUE; i <= MAX_VALUE; i += scale) {
            canvas.save();
            float angle = getValueAngle(i);
            canvas.rotate(START_ANGLE + angle, getWidth() / 2, getHeight() / 2);
            canvas.drawPath(scalePath, scalePaint);
            canvas.restore();
        }
    }

    private void drawRoundBg(Canvas canvas) {
        canvas.drawPath(bgPath, bgPaint);
    }

    private void drawNeedle(Canvas canvas) {
        canvas.save();
        float angle = getValueAngle(mAnimateValue);
        canvas.rotate(START_ANGLE + angle, getWidth() / 2, getHeight() / 2);
        canvas.drawPath(needlePath, bgPaint);
        canvas.restore();
    }

    private float getValueAngle(float value) {
        return ((value - MIN_VALUE) / (MAX_VALUE-MIN_VALUE)) * (END_ANGLE - START_ANGLE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);
        setMeasuredDimension(w, w);
    }
}
