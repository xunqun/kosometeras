package com.kdanmobile.kosometer.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.fragment.DashboardFragment;
import com.kdanmobile.kosometer.fragment.HistoryFragment;
import com.kdanmobile.kosometer.fragment.LastLocationFragment;
import com.kdanmobile.kosometer.fragment.NavigationDrawerFragment;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {


    private static final String FRAGMENT_DASHBOARD = "dashboard";
    private static final String FRAGMENT_HISTORY = "history";
    private static final String FRAGMENT_LASTLOCATION = "history";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private DashboardFragment mDashboardFragment;
    private HistoryFragment mHistoryFragment;
    private LastLocationFragment mLastLocationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }
    private void checkAndDoKeepScreenOn(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean keepScreenOn = sharedPref.getBoolean("keep_screen_on",false);
        if(keepScreenOn){
           getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAndDoKeepScreenOn();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (position) {
            case 0:

                if (mDashboardFragment == null) {
                    mDashboardFragment = DashboardFragment.newInstance(FRAGMENT_DASHBOARD);
                    mDashboardFragment.setOnFragmentInteractionCallback(new OnFragmentInteractionCallback() {
                        @Override
                        public void onAttached() {
                            onSectionAttached(1);
                            restoreActionBar();
                        }
                    });
                }
                fragmentManager.beginTransaction()
                        .replace(R.id.container, mDashboardFragment)
                        .commit();

                break;
            case 1:

                if (mHistoryFragment == null) {
                    mHistoryFragment = HistoryFragment.newInstance(FRAGMENT_HISTORY);
                    mHistoryFragment.setOnFragmentInteractionCallback(new OnFragmentInteractionCallback() {
                        @Override
                        public void onAttached() {
                            onSectionAttached(2);
                            restoreActionBar();
                        }
                    });
                }
                fragmentManager.beginTransaction()
                        .replace(R.id.container, mHistoryFragment)
                        .commit();
                break;
            case 2:
                mLastLocationFragment = (LastLocationFragment)fragmentManager.findFragmentByTag(FRAGMENT_LASTLOCATION);

                if (mLastLocationFragment == null) {
                    mLastLocationFragment = LastLocationFragment.newInstance(FRAGMENT_LASTLOCATION);
                    mLastLocationFragment.setOnFragmentInteractionCallback(new OnFragmentInteractionCallback() {
                        @Override
                        public void onAttached() {
                            onSectionAttached(3);
                            restoreActionBar();
                        }
                    });
                    fragmentManager.beginTransaction()

                            .replace(R.id.container, mLastLocationFragment, FRAGMENT_LASTLOCATION)
                            .commit();
                }else{

                    fragmentManager.beginTransaction()
                            .attach(mLastLocationFragment)
                            .commit();
                }


                break;
            case 3:
                Intent it = new Intent(this, SettingsActivity.class);
                startActivity(it);
                break;

        }


    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section_last_location);
                break;
        }

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionCallback {
        public void onAttached();
    }

}
