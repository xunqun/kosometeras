package com.kdanmobile.kosometer.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kdanmobile.kosometer.R;
import com.kdanmobile.kosometer.fingerchart.ChartView;
import com.kdanmobile.kosometer.model.DbHelper;
import com.kdanmobile.kosometer.model.RecordManager;
import com.kdanmobile.kosometer.model.RecycleAdapter;

import java.util.Calendar;

public class RecordActivity extends ActionBarActivity implements OnMapReadyCallback {

    public String TAG = "RecordActivity";

    // Required Intent Extra
    public static final String EXTRA_POSITION = "EXTRA_POSITION";

    // Views
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    RecycleAdapter mAdapter;
    RecordManager.RecordSet mRecordSet;
    TextView mDate;
    GoogleMap mMap;
    Marker mMarker;

    LatLng firstLatLng = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        int position = getIntent().getIntExtra(EXTRA_POSITION, -1);

        mRecordSet = RecordManager.get(this).getRecordSets().get(position);
        mRecordSet.retriveData(this);

        initViews();
        initMap();
    }


    private void initViews() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new RecycleAdapter(this, mRecordSet);
        mAdapter.setOnSelectedXListener(new ChartView.OnSelectedXListener() {
            @Override
            public void onSelected(double x, ChartView.ChartPoint point) {
                RecordManager.LocationData data = getApproximateX(x);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(data.getValue()));
                if (mMarker == null) {
                    mMarker = mMap.addMarker(new MarkerOptions()
                            .position(data.getValue())
                            .title("Hello world"));
                }
                mMarker.setPosition(data.getValue());
            }

            @Override
            public void onActionUp() {

            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mDate = (TextView)findViewById(R.id.date);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mRecordSet.getBeginTime());
        mDate.setText(cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DAY_OF_MONTH));

    }

    private RecordManager.LocationData getApproximateX(double d) {
        int position = binarySearch(0, mRecordSet.getLocations().size() - 1, d);
        return mRecordSet.getLocations().get(position);
    }

    private int binarySearch(int from, int to, double target) {
        int index = (int) ((from + to) / 2);
        if (index == from || index == to) {
            return index;
        }

        if (mRecordSet.getLocations().get(index).getTimestamp() == target) {
            return index;
        } else if (mRecordSet.getLocations().get(index).getTimestamp() > target) {
            return binarySearch(from, index, target);
        } else {
            return binarySearch(index, to, target);
        }
    }

    private void initMap() {
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    private void initPolyline() {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.width(5).color(Color.BLUE).geodesic(true);

        if (mRecordSet.getLocations() != null) {
            for (int i = 0; i < mRecordSet.getLocations().size(); i++) {
                RecordManager.LocationData d = mRecordSet.getLocations().get(i);
                lineOptions.add(d.getValue());
                Log.i(TAG, d.getValue().latitude + ", " + d.getValue().longitude);
                if (firstLatLng == null)
                    firstLatLng = d.getValue();
            }
            Polyline polyline = mMap.addPolyline(lineOptions);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstLatLng != null)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(firstLatLng, 16));
    }

    @Override
    protected void onPause() {
        super.onPause();
        DbHelper.closeDb();
    }

    @Override
    protected void onStop(){
        super.onStop();
        mRecordSet.dataRecycle();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        initPolyline();
    }
}
